from flask import Flask, render_template, request
from pyspark import SparkContext, SQLContext
from pyspark.sql.types import *
from pyspark.sql import functions as F
from pyspark.mllib.linalg import Vectors, VectorUDT
from pyspark.sql import Row
import numpy as np, json, pandas as pd
from pyspark.ml.feature import CountVectorizerModel, IDFModel
from flask_bootstrap import Bootstrap

app = Flask(__name__)
bootsrap = Bootstrap(app)

#Spark context initialized
sc = SparkContext()
sqlContext = SQLContext(sc)

#load idf and cv models used with datset
loadedCv = CountVectorizerModel.load("data/models/pipeline_test_count_vectorizer")
loadedIDF = IDFModel.load("data/models/pipeline_test_idf")

#load inverted index df
index_df = sqlContext.read.format("csv").option("header", "false").load("output_test_inverted_index/*.csv")
index_df = index_df.withColumn("term_id", index_df["_c0"].cast(IntegerType()))
index_df = index_df.withColumn("ev1", F.regexp_extract(index_df._c1, r"(\[\'d*(.*?)\])", 0))
index_df = index_df.withColumn("ev2", F.regexp_replace(index_df.ev1, r"\'|\[|\]", ""))
index_df = index_df.withColumn("doc_list", F.split(index_df["ev2"], ",\s").cast("array<int>").alias("doc_list"))

#load text dataset
frame = pd.read_csv("data/stackoverflow200.csv", index_col=None, header=1, encoding='utf-8')  # Prod
frame = frame.where(pd.notnull(frame), None)  # Fill NaN with Null

schema = StructType([
        StructField("q_id", StringType(), True),
        StructField("q_title", StringType(), True),
        StructField("q_body", StringType(), True),
        StructField("accepted_answer_id", IntegerType(), True),
        StructField("q_date", StringType(), True),
        StructField("q_type", StringType(), True),
        StructField("q_tags", StringType(), True),
        StructField("q_score", IntegerType(), True),
        StructField("a_id", IntegerType(), True),
        StructField("a_body", StringType(), True),
        StructField("a_creation_date", StringType(), True),
        StructField("a_type", StringType(), True),
        StructField("a_score", IntegerType(), True),
        StructField("a_tags", StringType(), True),
    ])

original_docs = sqlContext.createDataFrame(frame, schema)

#load vectorized documents
documents_df = sqlContext.read.format("csv").option("header", "false").load("output_test/*.csv")
documents_df = documents_df.withColumn("doc_id", documents_df["_c0"].cast(IntegerType()))

@app.route("/")
def index():
    headline = "NC Solution Search"
    query = request.form.get("query")
    return render_template("index.html", headline=headline)


@app.route("/search", methods=["POST"])
def search():
    #get query string, turn into array of terms (missing stemming and cleaning process)
    query_raw = request.form.get("query")
    query = query_raw.split()

    # turn into df query (maybe not neccesary)
    q = [(query,)]
    rdd = sc.parallelize(q)
    query = rdd.map(lambda x: Row(terms_clean=x[0]))
    query_df = sqlContext.createDataFrame(query)

    #transform query into tf-idf vector
    query_count = loadedCv.transform(query_df)
    query_idf = loadedIDF.transform(query_count)

    #get index of words from query
    udf_indices = F.udf(lambda x: x.indices.tolist(), ArrayType(IntegerType()))
    query_df = query_idf.withColumn("indices", udf_indices(query_idf["tf_idf_vector"]))

    #list of term indexes
    terms = query_df.select('indices').collect()
    term_list = terms[0].indices

    # -------------------- -------------------------------------------------------

    #search for documents with at least one term from query present using the index
    filtered_index_df = index_df.where(index_df.term_id.isin(term_list))
    index_df.show()

    # -------------------------------------------------------------------------------------
    # query vector prepaaration for calculating dot product
    query_vector = query_df.select('tf_idf_vector').collect()
    query_vector = query_vector[0].tf_idf_vector.toArray().tolist()
    print(type(query_vector))  # ML vector

    # document vectors
    doc_list_from_index = filtered_index_df.collect()
    doc_list = []
    for r in doc_list_from_index:
        doc_list = doc_list + r.doc_list

    print(doc_list)

    #filter vectorized documents
    final_docs = documents_df.where(documents_df.doc_id.isin(doc_list))
    final_docs.show()
    parse = F.udf(lambda s: Vectors.parse(s), VectorUDT())  # MLlib
    final_docs = final_docs.withColumn("vector", parse(final_docs["_c1"]))
    final_docs.show()

    # calculate dot product
    dot_udf = F.udf(lambda x: np.float32(x.dot(query_vector)).item(), DoubleType())
    final_df = final_docs.withColumn("dot_product", dot_udf(final_docs["vector"]))
    final_df = final_df.orderBy('dot_product', ascending=False).limit(10)

    # ------------------------------------------------------------------------------------------------------

    # retrieve document text by id
    documents = final_df.collect()
    top_10_documents = []
    for doc in documents:
        top_10_documents.append(doc.doc_id)
    results = original_docs.where(original_docs.q_id.isin(top_10_documents)).select(
        ["q_id", "q_title", "q_body", "a_body"])
    results.show()
    results = results.select(F.to_json(F.struct(*results.columns)).alias("json")) \
        .groupBy(F.spark_partition_id()) \
        .agg(F.collect_list("json").alias("json_list")).select(F.col("json_list").cast("string"))
    results = json.loads(results.collect()[0].json_list)
    print(type(results))

    return render_template("query.html", results = results, query=query_raw)