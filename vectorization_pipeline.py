import pandas as pd
from pyspark.ml.feature import StopWordsRemover, CountVectorizerModel, CountVectorizer, IDF, Tokenizer
from pyspark import SparkContext, SQLContext
from pyspark.ml import Pipeline, Transformer
from pyspark.sql.types import *
from typing import Iterable
from pyspark.sql import context, DataFrame, functions as F, DataFrameReader
import re
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords


def pre_process(string, stemmer):
  string = string.lower()
  string = re.sub("(\\W)+", " ", string)
  words = []
  for word in string.split(" "):
    if word is not "":
      words.append(stemmer.stem(word))
  return words

def array_to_string(my_list):
  return '[' + ','.join([str(elem) for elem in my_list]) + ']'

#---------------------------------CUSTOM TRANSFORMER: MERGE COLUMNS---------------------------------------------
class ColumnMerger(Transformer):

    def __init__(self, column_list: Iterable[str]):
        super(ColumnMerger, self).__init__()
        self.column_list = column_list

    def _transform(self, df: DataFrame) -> DataFrame:
        df = df.withColumn('merged_columns', F.concat(*[(F.col(x)) for x in self.column_list]))
        #df = df.withColumn('question', F.concat(F.col('q_title'), F.lit(' '), F.col('q_body')))
        return df

#---------------------------------CUSTOM TRANSFORMER: PRE PROCESS STRING---------------------------------------------
class PreProcesser(Transformer):

    def __init__(self):
        super(PreProcesser, self).__init__()

    def _transform(self, df: DataFrame) -> DataFrame:
        stemmer = PorterStemmer()
        split_udf = F.udf(lambda x: pre_process(x, stemmer), ArrayType(StringType()))
        df = df.withColumn("tokenized_string", split_udf(df["merged_columns"]))
        return df

#---------------------------------CUSTOM TRANSFORMER: PRE PROCESS STRING---------------------------------------------
class IDFtoString(Transformer):

    def __init__(self):
        super(IDFtoString, self).__init__()

    def _transform(self, df: DataFrame) -> DataFrame:
        array_to_string_udf = F.udf(array_to_string, StringType())
        df = df.withColumn('tf_idf_string', array_to_string_udf(df["tf_idf_vector"]))
        return df

#----------------------------------INITIALIZE CONTEXT AND LOAD CSV----------------------------------------------
sc = SparkContext()
sqlContext = SQLContext(sc)

frame = pd.read_csv("data/stackoverflow200.csv", index_col=None, header=1, encoding='utf-8') # Prod
frame = frame.where(pd.notnull(frame), None) # Fill NaN with Null

schema = StructType([
  StructField("q_id", StringType(), True),
  StructField("q_title", StringType(), True),
  StructField("q_body", StringType(), True),
  StructField("accepted_answer_id", IntegerType(), True),
  StructField("q_date", StringType(), True),
  StructField("q_type", StringType(), True),
  StructField("q_tags", StringType(), True),
  StructField("q_score", IntegerType(), True),
  StructField("a_id", IntegerType(), True),
  StructField("a_body", StringType(), True),
  StructField("a_creation_date", StringType(), True),
  StructField("a_type", StringType(), True),
  StructField("a_score", IntegerType(), True),
  StructField("a_tags", StringType(), True),
                      ])

df = sqlContext.createDataFrame(frame, schema)


#------------------------------------DEFINE PIPELINE STAGES-----------------------------------------------------------
#1 concat title and body
merger = ColumnMerger(column_list=["q_title","q_body"])
#2 tokenize and preprocess
tokenizer = PreProcesser()
#3 stopwords
remover = StopWordsRemover(inputCol="tokenized_string", outputCol="terms_clean", stopWords=stopwords.words('english'))
#5 countvectorizer
count_vectorizer = CountVectorizer(inputCol="terms_clean", outputCol="tf_vector")
#6 tf-idf vectorizer
idf_calculator = IDF(minDocFreq=1, inputCol="tf_vector", outputCol="tf_idf_vector")
#7 idf vector to string
idf_converter = IDFtoString()

#------------------------------------------ PIPELINE ------------------------------------------------------------------
pipeline = Pipeline(stages=[merger, tokenizer, remover, count_vectorizer, idf_calculator, idf_converter])

#fit pipeline
model = pipeline.fit(df)

#save count_vectorizer and idf models
model.stages[3].save("data/models/pipeline_test_count_vectorizer")
model.stages[4].save("data/models/pipeline_test_idf")

#transform df
df = model.transform(df)

#pick id and vector columns, save to csv
df = df.select(["q_id", "tf_idf_string"])
df.show()
df.write.csv("output_test")


