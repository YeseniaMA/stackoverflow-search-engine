from pyspark.ml import Pipeline, Transformer
from pyspark import SparkContext, SQLContext
from pyspark.sql.types import *
from pyspark.sql import functions as F
from pyspark.mllib.linalg import Vectors, VectorUDT, SparseVector

def array_to_string(my_list):
  return '[' + ','.join([str(elem) for elem in my_list]) + ']'

def dense_to_sparse(v):
    v = list(v)
    size = len(v)
    indices = []
    values = []
    count = 0
    for i in v:
        if i != 0 and count not in indices:
            values.append(i)
            indices.append(count)
        count += 1
    tuples = zip(indices, values)
    return SparseVector(size, tuples)

def indices(v):
    v = list(v)
    indices = []
    count = 0
    for i in v:
        if i != 0 and count not in indices:
            indices.append(count)
        count += 1
    return indices

#----------------------------------- Load vectorized documents dataset ---------------------------------
sc = SparkContext()
sqlContext = SQLContext(sc)
df = sqlContext.read.format("csv").option("header", "false").load("output_test/*.csv")
df.show()
#----------------------------------------------------------------------------------------------------------

#all udfs, maybe they wont work with a large dataset.

parse_udf = F.udf(lambda s: Vectors.parse(s), VectorUDT())
df_final = df.withColumn("tf_idf_dense", parse_udf(F.col("_c1")))

dense_to_sparse_udf = F.udf(lambda s: dense_to_sparse(s), VectorUDT())
indices_udf = F.udf(lambda s: indices(s), ArrayType(IntegerType()))
df_final = df_final.withColumn("sparse_v", dense_to_sparse_udf(F.col("tf_idf_dense")))

udf_indices = F.udf(lambda x: x.indices.tolist(), ArrayType(IntegerType()))
udf_values = F.udf(lambda x: x.values.tolist(), ArrayType(DoubleType()))

output_df = df_final.select(['_c0', 'sparse_v'])
output_df = output_df.withColumn("indices", udf_indices(output_df["sparse_v"]))
output_df.show()
df2 = output_df.select(output_df._c0, F.explode(output_df.indices))
df = df2.select(["col", "_c0"])

#any other way not using rdd??
rdd = df.rdd
rdd.map(lambda x: (x.col, x._c0))

rdd_final = rdd.groupByKey()
rdd_final.first()
inverted_index = rdd_final.toDF()

array_to_string_udf = F.udf(array_to_string, StringType())
inverted_index = inverted_index.withColumn('doc_list', array_to_string_udf(inverted_index["_2"]))

inverted_index.select(["_1", "doc_list"]).write.csv("output_test_inverted_index")