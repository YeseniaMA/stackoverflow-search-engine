
from pyspark.ml.feature import IDFModel, CountVectorizerModel, IDF, CountVectorizer, StopWordsRemover
from pyspark import SparkContext, SQLContext
from pyspark.sql.types import *
from pyspark.sql import context, udf, functions as F
import json, pandas as pd
from pyspark.mllib.linalg import Vectors, VectorUDT, SparseVector, DenseVector
import re
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords

def pre_process(string, stemmer):
  string = string.lower()
  string = re.sub("(\\W)+", " ", string)
  words = []
  for word in string.split(" "):
    if word is not "":
      words.append(stemmer.stem(word))
  return words

sc = SparkContext()
sqlContext = SQLContext(sc)

#1 Get the query ----------------------------------------------
query = "html json"

#2 convert to tf_idf vector -----------------------------------
#2a load models
idf_model = IDFModel.load("models/idf-model-2000")
cv_model = CountVectorizerModel.load("models/count-vectorizer-model-2000")


data = [(query,)]
rdd = sc.parallelize(data)

df = rdd.toDF()
df.show()
df = df.selectExpr("_1 as query")

stemmer = PorterStemmer()
split_udf = F.udf(lambda x: pre_process(x, stemmer), ArrayType(StringType()))
new_df = df.withColumn("tokenized_string", split_udf(df["query"]))

remover = StopWordsRemover(inputCol="tokenized_string", outputCol="terms_clean", stopWords=stopwords.words('english'))
clean_df = remover.transform(new_df)

clean_df.show()

tf_df = cv_model.transform(clean_df)
tf_df.show()

tf_idf = idf_model.transform(tf_df)
tf_idf.show()

#list of word indexes ---------------------------------------------
udf_indices = F.udf(lambda x: x.indices.tolist(), ArrayType(IntegerType()))
output_df = tf_idf.withColumn("indices", udf_indices(tf_idf["tf_idf_vector"]))

#get list of documents with same words
    #loop throguh words
doc_list_string = []

schema = StructType([
  StructField("term_id", IntegerType(), True),
  StructField("doc_list", StringType(), True)
                      ])

frame1 = pd.read_csv("inverted_index_200/part-00000-6ec39ac3-a97d-4ee5-8962-8f04d5114762-c000.csv", index_col=None, header=0, encoding='utf-8') # Prod
frame1 = frame1.where(pd.notnull(frame1), None) # Fill NaN with Null
df1 = sqlContext.createDataFrame(frame1, schema)

frame2 = pd.read_csv("inverted_index_200/part-00001-6ec39ac3-a97d-4ee5-8962-8f04d5114762-c000.csv", index_col=None, header=0, encoding='utf-8') # Prod
frame2 = frame2.where(pd.notnull(frame2), None) # Fill NaN with Null
df2 = sqlContext.createDataFrame(frame2, schema)


frame3 = pd.read_csv("inverted_index_200/part-00002-6ec39ac3-a97d-4ee5-8962-8f04d5114762-c000.csv", index_col=None, header=0, encoding='utf-8') # Prod
frame3 = frame3.where(pd.notnull(frame3), None) # Fill NaN with Null
df3 = sqlContext.createDataFrame(frame3, schema)

frame4 = pd.read_csv("inverted_index_200/part-00003-6ec39ac3-a97d-4ee5-8962-8f04d5114762-c000.csv", index_col=None, header=0, encoding='utf-8') # Prod
frame4 = frame4.where(pd.notnull(frame4), None) # Fill NaN with Null
df4 = sqlContext.createDataFrame(frame4, schema)

frame5 = pd.read_csv("inverted_index_200/part-00004-6ec39ac3-a97d-4ee5-8962-8f04d5114762-c000.csv", index_col=None, header=0, encoding='utf-8') # Prod
frame5 = frame5.where(pd.notnull(frame5), None) # Fill NaN with Null
df5 = sqlContext.createDataFrame(frame5, schema)

frame6 = pd.read_csv("inverted_index_200/part-00005-6ec39ac3-a97d-4ee5-8962-8f04d5114762-c000.csv", index_col=None, header=0, encoding='utf-8') # Prod
frame6 = frame6.where(pd.notnull(frame6), None) # Fill NaN with Null
df6 = sqlContext.createDataFrame(frame6, schema)

frame7 = pd.read_csv("inverted_index_200/part-00006-6ec39ac3-a97d-4ee5-8962-8f04d5114762-c000.csv", index_col=None, header=0, encoding='utf-8') # Prod
frame7 = frame7.where(pd.notnull(frame7), None) # Fill NaN with Null
df7 = sqlContext.createDataFrame(frame7, schema)

frame8 = pd.read_csv("inverted_index_200/part-00007-6ec39ac3-a97d-4ee5-8962-8f04d5114762-c000.csv", index_col=None, header=0, encoding='utf-8') # Prod
frame8 = frame8.where(pd.notnull(frame8), None) # Fill NaN with Null
df8 = sqlContext.createDataFrame(frame8, schema)


df = df1.union(df2)
df = df.union(df3)
df = df.union(df4)
df = df.union(df5)
df = df.union(df6)
df = df.union(df7)
df = df.union(df8)
    #parse string

#get vectors of documents
    #loop through id's
    #get from questions vectorized
    #parse vectors

#calculate dot product
    #df with ids and vectors
    #dot product in new col
    #sort

#get 5 documents from stackoverflow