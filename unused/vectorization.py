import pandas as pd
from pyspark.ml.feature import StopWordsRemover, CountVectorizerModel, CountVectorizer, IDF
from pyspark import SparkContext, SQLContext
from pyspark.sql.types import *
from pyspark.sql import context, udf, functions as F
import re
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords

def array_to_string(my_list):
  return '[' + ','.join([str(elem) for elem in my_list]) + ']'

def pre_process(string, stemmer):
  string = string.lower()
  string = re.sub("(\\W)+", " ", string)
  words = []
  for word in string.split(" "):
    if word is not "":
      words.append(stemmer.stem(word))
  return words

frame = pd.read_csv("stackoverflow200.csv", index_col=None, header=1, encoding='utf-8') # Prod
frame = frame.where(pd.notnull(frame), None) # Fill NaN with Null

schema = StructType([
  StructField("q_id", IntegerType(), True),
  StructField("q_title", StringType(), True),
  StructField("q_body", StringType(), True),
  StructField("accepted_answer_id", IntegerType(), True),
  StructField("q_date", StringType(), True),
  StructField("q_type", StringType(), True),
  StructField("q_tags", StringType(), True),
  StructField("q_score", IntegerType(), True),
  StructField("a_id", IntegerType(), True),
  StructField("a_body", StringType(), True),
  StructField("a_creation_date", StringType(), True),
  StructField("a_type", StringType(), True),
  StructField("a_score", IntegerType(), True),
  StructField("a_tags", StringType(), True),
                      ])

sc = SparkContext()
sqlContext = SQLContext(sc)
df = sqlContext.createDataFrame(frame, schema)
df = df.withColumn('question', F.concat(F.col('q_title'), F.lit(' '), F.col('q_body')))
df = df.select(["q_id", "question"])
df.show()

#DATAFRAME PREPARATION [DONE]
stemmer = PorterStemmer()
split_udf = F.udf(lambda x: pre_process(x, stemmer), ArrayType(StringType()))
new_df = df.withColumn("tokenized_string", split_udf(df["question"]))

remover = StopWordsRemover(inputCol="tokenized_string", outputCol="terms_clean", stopWords=stopwords.words('english'))
clean_df = remover.transform(new_df)

clean_df = clean_df.select(["q_id", "terms_clean"])
clean_df.show()

cv = CountVectorizer(inputCol="terms_clean", outputCol="tf_vector")
model = cv.fit(clean_df)
model.save("models/count-vectorizer-model-200")
tf_df = model.transform(clean_df)
tf_df.show()

# IDF VECTOR [DONE]
idf = IDF(minDocFreq=1, inputCol="tf_vector", outputCol="tf_idf_vector")
idf_model = idf.fit(tf_df)
idf_model.save("models/idf-model-200")

#TF_IDF VECTORS [DONE]
tf_idf = idf_model.transform(tf_df)
tf_idf.show()

array_to_string_udf = F.udf(array_to_string, StringType())
tf_idf = tf_idf.withColumn('tf_idf_string', array_to_string_udf(tf_idf["tf_idf_vector"]))

tf_idf.select(["q_id", "tf_idf_string"]).write.csv("questions_vectorized_200")