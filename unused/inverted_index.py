import pandas as pd
from pyspark import SparkContext, SQLContext
from pyspark.sql.types import *
from pyspark.sql import context, udf, functions as F
import json
from pyspark.mllib.linalg import Vectors, VectorUDT, SparseVector, DenseVector

def parse_embedding_from_string(x):
    res = json.loads(x)
    return res

def array_to_string(my_list):
  return '[' + ','.join([str(elem) for elem in my_list]) + ']'

def dense_to_sparse(v):
    v = list(v)
    size = len(v)
    indices = []
    values = []
    count = 0
    for i in v:
        if i != 0 and count not in indices:
            values.append(i)
            indices.append(count)
        count += 1
    tuples = zip(indices, values)
    return SparseVector(size, tuples)

def indices(v):
    v = list(v)
    indices = []
    count = 0
    for i in v:
        if i != 0 and count not in indices:
            indices.append(count)
        count += 1
    return indices

retrieve_embedding_udf = F.udf(parse_embedding_from_string, ArrayType(DoubleType()))

sc = SparkContext()
sqlContext = SQLContext(sc)

schema = StructType([
  StructField("q_id", IntegerType(), True),
  StructField("tf_idf_string", StringType(), True)
                      ])

frame1 = pd.read_csv("questions_vectorized_200/part-00000-447f2c4c-f7e6-415e-9ab3-307127fd9cae-c000.csv", index_col=None, header=0, encoding='utf-8') # Prod
frame1 = frame1.where(pd.notnull(frame1), None) # Fill NaN with Null
df1 = sqlContext.createDataFrame(frame1, schema)

frame2 = pd.read_csv("questions_vectorized_200/part-00001-447f2c4c-f7e6-415e-9ab3-307127fd9cae-c000.csv", index_col=None, header=0, encoding='utf-8') # Prod
frame2 = frame2.where(pd.notnull(frame2), None) # Fill NaN with Null
df2 = sqlContext.createDataFrame(frame2, schema)


#frame3 = pd.read_csv("questions_vectorized_2000/part-00002-4e27ca22-84f8-47e6-8489-d01acb020846-c000.csv", index_col=None, header=0, encoding='utf-8') # Prod
#frame3 = frame1.where(pd.notnull(frame3), None) # Fill NaN with Null
#df3 = sqlContext.createDataFrame(frame3, schema)

df = df1.union(df2)
#df = df.union(df3)


parse_udf = F.udf(lambda s: Vectors.parse(s), VectorUDT())
df_final = df.withColumn("tf_idf_dense", parse_udf(F.col("tf_idf_string")))

dense_to_sparse_udf = F.udf(lambda s: dense_to_sparse(s), VectorUDT())
#indices_udf = F.udf(lambda s: indices(s), ArrayType(IntegerType()))
df_final = df_final.withColumn("sparse_v", dense_to_sparse_udf(F.col("tf_idf_dense")))

#df_final.show()

udf_indices = F.udf(lambda x: x.indices.tolist(), ArrayType(IntegerType()))
#udf_values = F.udf(lambda x: x.values.tolist(), ArrayType(DoubleType()))

output_df = df_final.select(['q_id', 'sparse_v'])
output_df = output_df.withColumn("indices", udf_indices(output_df["sparse_v"]))
#output_df.show()
df2 = output_df.select(output_df.q_id, F.explode(output_df.indices))
df = df2.select(["col", "q_id"])

rdd = df.rdd
rdd.map(lambda x: (x.col, x.q_id))

rdd_final = rdd.groupByKey()
rdd_final.first()
inverted_index = rdd_final.toDF()
#print(rdd_final.take(5))

array_to_string_udf = F.udf(array_to_string, StringType())
inverted_index = inverted_index.withColumn('doc_list', array_to_string_udf(inverted_index["_2"]))

inverted_index.select(["_1", "doc_list"]).write.csv("inverted_index_200")